const axios = require("axios");

const apiAdapter = (baseURL) => {
    return axios.create({
        baseURL: baseURL,
        timeout: parseInt(process.env.TIMEOUT),
    });
};

module.exports = {
    apiAdapter
};