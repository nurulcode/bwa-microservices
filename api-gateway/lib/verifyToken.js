const jwt = require("jsonwebtoken");

const verifyToken = async (req, res, next) => {
    const token = req.headers.authorization;
    jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
        if (err) {
            return res.status(403).json({ message: err.message });
        }

        req.user = decoded;
        return next();
    });
};

const verifyRefreshToken = (refreshToken) => {
    return jwt.verify(
        refreshToken,
        process.env.JWT_SECRET_REFRESH_TOKEN,
        (err, decode) => {
            if (err) {
                return res.status(401).json({
                    status: "error",
                    message: "Invalid refresh token",
                });
            }

            return decode;
        }
    );
};

const signToken = (token) => {
    return jwt.sign({ data: token }, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_ACCESS_TOKEN_EXPIRED, // '5m' adalah nilai valid
    });
};

const signRefreshToken = (token) => {
    return jwt.sign({ data: token }, process.env.JWT_SECRET_REFRESH_TOKEN, {
        expiresIn: process.env.JWT_REFRESH_TOKEN_EXPIRED, // '5m' adalah nilai valid
    });
};

module.exports = {
    verifyRefreshToken,
    signToken,
    verifyToken,
    signRefreshToken,
};
