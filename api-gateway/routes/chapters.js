var express = require("express");
const chaptersControlelr = require("../controllers/chapters");
var router = express.Router();

/* GET mentors page. */
router.post("/",  chaptersControlelr.create);
router.get("/",  chaptersControlelr.getAll);
router.get("/:id",  chaptersControlelr.get);
router.put("/:id",  chaptersControlelr.update);
router.delete("/:id",  chaptersControlelr.destroy);

module.exports = router;
