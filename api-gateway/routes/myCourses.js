var express = require("express");
const myCoursesControlelr = require("../controllers/myCourses");
const { verifyToken } = require("../lib/verifyToken");
var router = express.Router();

/* GET mentors page. */
router.post("/", verifyToken, myCoursesControlelr.create);
router.get("/:id", verifyToken, myCoursesControlelr.get);

module.exports = router;
