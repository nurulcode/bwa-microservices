const express = require('express');
const mediaControlelr = require('../controllers/medias');
const router = express.Router();

/* GET courses page. */
router.post('/', mediaControlelr.create)
router.get('/', mediaControlelr.getAll)
router.delete('/:id', mediaControlelr.destroy)

module.exports = router;
