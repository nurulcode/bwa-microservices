var express = require("express");
const imageCoursesControlelr = require("../controllers/imageCourses");
var router = express.Router();

/* GET mentors page. */
router.post("/", imageCoursesControlelr.create);
router.delete("/:id", imageCoursesControlelr.destroy);

module.exports = router;
