var express = require("express");
const coursesControlelr = require("../controllers/courses");
const { permission } = require("../lib/permission");
var router = express.Router();
const { verifyToken } = require("../lib/verifyToken");

/* GET mentors page. */
router.get("/", coursesControlelr.getAll);
router.get("/:id", coursesControlelr.get);

router.post("/", verifyToken, permission("admin"), coursesControlelr.create);
router.put("/:id", verifyToken, permission("admin"), coursesControlelr.update);
router.delete(
    "/:id",
    verifyToken,
    permission("admin"),
    coursesControlelr.destroy
);

module.exports = router;
