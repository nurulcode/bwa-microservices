var express = require("express");
const reviewsControlelr = require("../controllers/reviews");
const { verifyToken } = require("../lib/verifyToken");
var router = express.Router();

/* GET mentors page. */
router.post("/", verifyToken, reviewsControlelr.create);
router.put("/:id", verifyToken, reviewsControlelr.update);
router.delete("/:id", verifyToken, reviewsControlelr.destroy);

module.exports = router;
