const express = require("express");
const userController = require("../controllers/users");
const { verifyToken } = require("../lib/verifyToken");
const router = express.Router();

router.post("/register", userController.register);
router.post("/login", userController.login);
router.post("/logout", verifyToken, userController.logout);
router.put("/", verifyToken, userController.update);
router.get("/", verifyToken, userController.getUser);

module.exports = router;
