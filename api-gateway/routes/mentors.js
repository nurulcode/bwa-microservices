var express = require("express");
const mentorsControlelr = require("../controllers/mentors");
var router = express.Router();

/* GET mentors page. */
router.post("/", mentorsControlelr.create);
router.get("/", mentorsControlelr.getAll);
router.get("/:id", mentorsControlelr.get);
router.put("/:id", mentorsControlelr.update);
router.delete("/:id", mentorsControlelr.destroy);

module.exports = router;
