const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
require("dotenv").config();

const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");
const refreshTokensRouter = require("./routes/refreshTokens");
const coursesRouter = require("./routes/courses");
const mentorsRouter = require("./routes/mentors");
const mediasRouter = require("./routes/medias");
const myCoursesRouter = require("./routes/myCourses");
const webhookRouter = require("./routes/webhook");
const chaptersRouter = require("./routes/chapters");
const imageCoursesRouter = require("./routes/imageCourses");
const reviewsRouter = require("./routes/reviews");

const app = express();

app.use(logger("dev"));
app.use(express.json({ limit: "50mb" }));
app.use(express.urlencoded({ extended: false, limit: "50mb" }));
app.use(cookieParser());
// app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/refresh-tokens", refreshTokensRouter);
app.use("/courses", coursesRouter);
app.use("/courses/mentors", mentorsRouter);
app.use("/courses/image-courses", imageCoursesRouter);
app.use("/courses/chapters", chaptersRouter);
app.use("/courses/reviews", reviewsRouter);

app.use("/my-courses", myCoursesRouter);
app.use("/webhook", webhookRouter);
app.use("/media", mediasRouter);


module.exports = app;
