const { apiAdapter } = require("../../lib/apiAdapter");
const jwt = require("jsonwebtoken");
const { signToken, signRefreshToken } = require("../../lib/verifyToken");

const { URL_SERVICE_USER } = process.env;

const api = apiAdapter(URL_SERVICE_USER);

const login = async (req, res) => {
    try {
        const user = await api.post("/users/login", req.body);
        const data = user?.data?.data;

        const token = signToken(data);
        const refreshToken = signRefreshToken(data);

        await api.post("/refresh-tokens", {
            refresh_token: refreshToken,
            user_id: data.id,
        });

        return res.json({
            status: "success",
            data: {
                token: token,
                refresh_token: refreshToken,
            },
        });
    } catch (error) {
        if (error.code === "ECONNREFUSED") {
            return res.status(503).json({
                status: "error",
                message: "Service unavailable",
            });
        }

        const { status, data } = error.response || {};
        return res.status(status || 500).json({
            status: "error",
            message: data?.message || "Something went wrong",
        });
    }
};

module.exports = login;
