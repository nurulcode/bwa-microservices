const getUser = require("./getUser");
const login = require("./login");
const logout = require("./logout");
const register = require("./register");
const update = require("./update");

module.exports = {
    register,
    getUser,
    login,
    logout,
    update
}