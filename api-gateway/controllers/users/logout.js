const { apiAdapter } = require("../../lib/apiAdapter");
const { URL_SERVICE_USER } = process.env;

const api = apiAdapter(URL_SERVICE_USER);

const logout = async (req, res) => {
    try {
        const id = req.user?.data?.id
        const user = await api.post("/users/logout", {
            user_id : id,
        });
        return res.json(user.data);
    } catch (error) {
        if (error.code === "ECONNREFUSED") {
            return res.status(503).json({
                status: "error",
                message: "Service unavailable",
            });
        }

        const { status, data } = error.response || {};
        return res.status(status || 500).json({
            status: "error",
            message: data?.message || "Something went wrong",
        });
    }
};

module.exports = logout;
