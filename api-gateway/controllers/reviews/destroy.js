const { apiAdapter } = require("../../lib/apiAdapter");
const { URL_SERVICE_COURSE } = process.env;

const api = apiAdapter(URL_SERVICE_COURSE);

const destroy = async (req, res) => {
    try {
        const id = req.params.id
        const review = await api.delete(`/api/reviews/${id}`);
        return res.json(review.data);
    } catch (error) {
        if (error.code === "ECONNREFUSED") {
            return res.status(503).json({
                status: "error",
                message: "Service unavailable",
            });
        }

        const { status, data } = error.response || {};
        return res.status(status || 500).json({
            status: "error",
            message: data?.message || "Something went wrong",
        });
    }
};

module.exports = destroy;
