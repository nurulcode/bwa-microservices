const { apiAdapter } = require("../../lib/apiAdapter");
const { URL_SERVICE_ORDER_PAYMENT } = process.env;
const amqp = require("amqplib");

const api = apiAdapter(URL_SERVICE_ORDER_PAYMENT);

const service = async (req, res) => {
    try {
        const webhook = await api.post("/api/webhook", req.body);

        if (webhook == 'ok') {
            const RABBITMQ_URL = "amqp://localhost";
            const ROUTING_KEY = "notification_service";
            const EXCHANGE_NAME = "notification_exchange";
            const QUEUE_NAME = "notification_queue";

            // const connection = await amqp.connect(RABBITMQ_URL);
            // const channel = await connection.createChannel();
            // await channel.assertExchange(EXCHANGE_NAME, "direct", {
            //     durable: false,
            // });
            // const message = JSON.stringify({
            //     event: "CREATE_NOTIFICATION",
            //     data: req.body,
            // });

            // channel.publish(EXCHANGE_NAME, ROUTING_KEY, Buffer.from(message));

            // console.log(" [x] Sent %s", message);

            // setTimeout(() => {
            //     connection.close();
            // }, 500);

            amqp.connect(RABBITMQ_URL).then((conn) => {
                return conn.createChannel().then((channel) => {
                    const assert = channel.assertQueue(QUEUE_NAME , {
                        durable: false,
                    });

                    return assert.then(() => {
                        const message = JSON.stringify({
                            "event" : "CREATE_NOTIFICATION",
                            "data" : req.body
                        }); // Pastikan objek JSON diubah menjadi string
                        channel.sendToQueue(QUEUE_NAME , Buffer.from(message), { persistent: true });
                        console.log(`[x] Sent tayo` + message);
                        return channel.close();
                    });
                });
            });
        }

        return res.json("ok");
    } catch (error) {
        if (error.code === "ECONNREFUSED") {
            return res.status(503).json({
                status: "error",
                message: "Service unavailable",
            });
        }

        const { status, data } = error.response || {};
        return res.status(status || 500).json({
            status: "error",
            message: error.message,
        });
    }
};

module.exports = service;
