const { apiAdapter } = require("../../lib/apiAdapter");
const { URL_SERVICE_COURSE } = process.env;

const api = apiAdapter(URL_SERVICE_COURSE);

const create = async (req, res) => {
    try {
        const myCourses = await api.post("/api/my-courses", {
            user_id : req.user.data.id,
            course_id : req.body.course_id
        });
        return res.json(myCourses.data);
    } catch (error) {
        if (error.code === "ECONNREFUSED") {
            return res.status(503).json({
                status: "error",
                message: "Service unavailable",
            });
        }

        const { status, data } = error.response || {};
        return res.status(status || 500).json({
            status: "error",
            message: data?.message || "Something went wrong",
        });
    }
};

module.exports = create;
