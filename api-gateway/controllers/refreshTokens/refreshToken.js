const { apiAdapter } = require("../../lib/apiAdapter");
const { verifyRefreshToken, signToken } = require("../../lib/verifyToken");

const {
    URL_SERVICE_USER,
} = process.env;

const api = apiAdapter(URL_SERVICE_USER);

const refreshToken = async (req, res) => {
    try {
        const refToken = req.body.refresh_token;
        const email = req.body.email;

        if (!refToken || !email) {
            return res.status(400).json({
                status: "error",
                message: "Invalid refresh token provided",
            });
        }

        await api.get("/refresh-tokens", {
            params: { refresh_token: refToken },
        });

        const verify = verifyRefreshToken(refToken);

        if (verify.data.email !== email) {
            return res.status(401).json({
                status: "error",
                message: "email does not match",
            });
        }

        return res.json({
            status: "success",
            data: {
                token: signToken(email),
            },
        });
    } catch (error) {
        if (error.code === "ECONNREFUSED") {
            return res.status(503).json({
                status: "error",
                message: "Service unavailable",
            });
        }

        const { status, data } = error.response || {};
        return res.status(status || 500).json({
            status: "error",
            message: data?.message || "Something went wrong",
        });
    }
};

module.exports = refreshToken
