const { apiAdapter } = require("../../lib/apiAdapter");
const { URL_SERVICE_MEDIA } = process.env;

const api = apiAdapter(URL_SERVICE_MEDIA);

const getAll = async (req, res) => {
    try {
        const media = await api.get('/media');
        return res.json(media.data);
    } catch (error) {
        if (error.code === "ECONNREFUSED") {
            return res.status(503).json({
                status: "error",
                message: "Service unavailable",
            });
        }

        const { status, data } = error.response;
        return res.status(status).json({
            status: "error",
            message: data.message,
        });
    }
};

module.exports = getAll;
