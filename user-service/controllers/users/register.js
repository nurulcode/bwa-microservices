const bcryptjs = require("bcryptjs");
const { User } = require("../../models");
const fastestValidator = require("fastest-validator");
const v = new fastestValidator();

module.exports = register = async (req, res) => {
    const request = await registerRequest(req);
    if (request.length) {
        return res.status(400).json({
            status: "error",
            message: request,
        });
    }

    const email = await findByEmail(req);
    if (email) {
        return res.status(400).json({
            status: "error",
            message: "email already exists",
        });
    }

    const data = {
        name: req.body.name,
        email: req.body.email,
        password: await hashedPassword(req),
        profession: req.body.profession,
        role: "student",
    };

    const create = await User.create(data);

    return res.status(201).json({
        status: "success",
        data: create,
    });
};

const registerRequest = (req) => {
    const schema = {
        name: "string|empty:false",
        email: "email|empty:false",
        password: "string|empty:false",
        profession: "string|optional",
    };

    return v.validate(req.body, schema);
};

const findByEmail = (req) => {
    return User.findOne({
        where: { email: req.body.email },
    });
};

const hashedPassword = (req) => {
    return bcryptjs.hashSync(req.body.password, 10);
};
