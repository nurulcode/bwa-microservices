const bcryptjs = require("bcryptjs");
const { User } = require("../../models");
const fastestValidator = require("fastest-validator");
const v = new fastestValidator();

module.exports = update = async (req, res) => {
    const request = await updateRequest(req);
    if (request.length) {
        return res.status(400).json({
            status: "error",
            message: request,
        });
    }

    const user = await findByid(req.params.id);
    if (!user) {
        return res.status(400).json({
            status: "error",
            message: "user not found",
        });
    }

    const email = await findByEmail(req.body.email);
    if (!email) {
        return res.status(400).json({
            status: "error",
            message: "email not found",
        });
    }

    if (email && req.body.email !== user.email) {
        return res.status(400).json({
            status: "error",
            message: "email not found",
        });
    }

    const data = {
        name: req.body.name,
        email: req.body.email,
        password: await hashedPassword(req),
        profession: req.body.profession,
        avatar: req.body.avatar,
    };

   await user.update(data);

    return res.status(201).json({
        status: "success",
        data: {
            name: user.name,
            email: user.email,
            profession: user.profession,
            avatar: user.avatar,
        },
    });
};

const updateRequest = (req) => {
    const schema = {
        name: "string|empty:false",
        email: "email|empty:false",
        password: "string|empty:false",
        profession: "string|optional",
        avatar: "string|optional",
    };

    return v.validate(req.body, schema);
};

const findByid = (id) => {
    return User.findByPk(id);
};

const findByEmail = (email) => {
    return User.findOne({
        where: { email },
    });
};

const hashedPassword = (req) => {
    return bcryptjs.hashSync(req.body.password, 10);
};


