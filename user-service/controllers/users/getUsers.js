const { User } = require("../../models");

module.exports = update = async (req, res) => {
    const userId = req.query.user_id || [];

    const sql = {
        attributes: ["id", "name", "email", "role", "profession", "avatar"],
    };

    if (userId.length) {
        sql.where = {
            id: userId,
        };
    }

    const users = await User.findAll(sql);

    return res.json({
        status: "success",
        data: users,
    });
};

