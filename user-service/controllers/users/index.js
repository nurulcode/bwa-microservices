const getUsers = require("./getUsers");
const getUser = require("./getUser");
const login = require("./login");
const register = require("./register");
const update = require("./update");
const logout = require("./logout");

module.exports = {
    register,
    update,
    login,
    getUser,
    getUsers,
    logout
};
