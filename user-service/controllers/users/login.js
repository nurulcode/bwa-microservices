const bcryptjs = require("bcryptjs");
const { User } = require("../../models");
const fastestValidator = require("fastest-validator");
const v = new fastestValidator();

module.exports = login = async (req, res) => {
    const request = await loginRequest(req);
    if (request.length) {
        return res.status(400).json({
            status: "error",
            message: request,
        });
    }

    const user = await findByEmail(req);
    if (!user) {
        return res.status(400).json({
            status: "error",
            message: "email does not exist",
        });
    }

    const compare = isValidPassword(req.body.password, user.password);
    if (!compare) {
        return res.status(400).json({
            status: "error",
            message: "invalid password",
        });
    }

    return res.status(201).json({
        status: "success",
        data: {
            id: user.id,
            name: user.name,
            email: user.email,
            profession: user.profession,
            role: user.role,
        },
    });
};

const loginRequest = (req) => {
    const schema = {
        email: "email|empty:false",
        password: "string|empty:false",
    };

    return v.validate(req.body, schema);
};

const findByEmail = (req) => {
    return User.findOne({
        where: { email: req.body.email },
    });
};

const isValidPassword = (password, hash) => {
    return bcryptjs.compare(password, hash);
};
