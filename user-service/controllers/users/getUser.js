const { User } = require("../../models");

module.exports = update = async (req, res) => {
    const user = await findByid(req.params.id);

    if (!user) {
        return res.status(404).json({
            status: "error",
            message: "user not found",
        });
    }

    return res.json({
        status: "success",
        data: user,
    });
};

const findByid = (id) => {
    return User.findByPk(id, {
        attributes: ["id", "name", "email", "role", "profession", "avatar"],
    });
};
