const { RefreshToken } = require("../../models");

module.exports = getToken = async (req, res) => {
    const refreshToken = req.query.refresh_token;

    const token = await findByRefreshToken(refreshToken);
    if (!token) {
        return res.status(400).json({
            status: "error",
            message: "token not found",
        });
    }

    return res.status(201).json({
        status: "success",
        token,
    });
};

const findByRefreshToken = (token) => {
    return RefreshToken.findOne({
        where: { token },
    });
};
