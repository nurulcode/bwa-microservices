const bcryptjs = require("bcryptjs");
const { User, RefreshToken } = require("../../models");
const fastestValidator = require("fastest-validator");
const v = new fastestValidator();

module.exports = create = async (req, res) => {
    const request = await updateRequest(req);
    if (request.length) {
        return res.status(400).json({
            status: "error",
            message: request,
        });
    }

    const userId = req.body.user_id;
    const refreshToken = req.body.refresh_token;

    const user = await findByid(userId);
    if (!user) {
        return res.status(400).json({
            status: "error",
            message: "user not found",
        });
    }

    const data = {
        token: refreshToken,
        user_id: userId,
    };

    const refUser = await findByUserId(userId);
    let token = ""
    if (refUser) {
        token = await refUser.update(data);
    } else {
        token = await RefreshToken.create(data);
    }

    return res.status(201).json({
        status: "success",
        data: {
            id: token.id,
        },
    });
};

const updateRequest = (req) => {
    const schema = {
        user_id: "number",
        refresh_token: "string",
    };

    return v.validate(req.body, schema);
};

const findByid = (id) => {
    return User.findByPk(id);
};

const findByUserId = (id) => {
    return RefreshToken.findOne({
        user_id: { id },
    });
};
