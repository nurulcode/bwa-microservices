var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.json({
    status: "success",
    data: {
      name: process.env.APP_NAME,
      version: process.env.APP_VERSION
    }
  })
});

module.exports = router;
