const express = require("express");
const refreshTokenController = require("../controllers/refresh-token");
const router = express.Router();

router.post("/", refreshTokenController.create);
router.get("/", refreshTokenController.getToken);

module.exports = router;
