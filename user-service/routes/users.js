const express = require("express");
const userController = require("../controllers/users");
const router = express.Router();

router.post("/register", userController.register);
router.post("/login", userController.login);
router.post("/logout", userController.logout);
router.put("/:id", userController.update);
router.get("/:id", userController.getUser);
router.get("/", userController.getUsers);

module.exports = router;
