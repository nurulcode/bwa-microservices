"use strict";

const bcryptjs = require("bcryptjs");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.bulkInsert("users", [
            {
                name: "John Doe",
                role: "admin",
                email: "john@example.com",
                password: await bcryptjs.hashSync("secret123", 10),
                created_at: new Date(),
                updated_at: new Date(),
            },
            {
                name: "Nurul Hidayat",
                role: "student",
                email: "nurul@example.com",
                password: await bcryptjs.hashSync("secret123", 10),
                created_at: new Date(),
                updated_at: new Date(),
            },
        ]);
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete("users", null, {});
    },
};
