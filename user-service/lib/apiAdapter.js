const axios = require("axios");

const apiAdapter = async (baseURL) => {
    return await axios.create({
        baseURL: baseURL,
        timeout: process.env.AXIOS_TIMEOUT,
    });
};


module.exports = {
    apiAdapter
}