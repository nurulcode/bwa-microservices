const express = require("express");
const fs = require("fs");
const isBase64 = require("is-base64");
const base64Img = require("base64-img");
const { Media } = require("../models");
const router = express.Router();

/* GET medias page. */
router.get("/", async function (req, res, next) {
    const medias = await Media.findAll({
        attributes: ['id', 'image']
    });

    const mappedMedia = medias.map((e) => {
        e.image = `${req.get("host")}/${e.image}`;
        return e;
    });

    return res.json({
        status: "success",
        data: mappedMedia,
    });
});

/* POST medias page. */
router.post("/", function (req, res, next) {
    const image = req.body.image;

    if (!isBase64(image, { mimeRequired: true })) {
        return res.status(400).json({
            status: "error",
            message: "invalid base64",
        });
    }

    base64Img.img(
        image,
        "./public/images/" + formattedToday(),
        Date.now(),
        async (err, filepath) => {
            if (err) {
                return res.status(400).json({
                    status: "error",
                    message: err.message,
                });
            }

            const filename = filepath.split("/").pop();

            const media = await Media.create({
                image: `images/${formattedToday()}/${filename}`,
            });

            return res.json({
                status: "success",
                data: {
                    id: media.id,
                    image: `${process.env.HOSTNAME}/images/${filename}`,
                },
            });
        }
    );
});

/* DELETE medias page. */
router.delete("/:id", async (req, res, next) => {
    const id = req.params.id;

    const media = await Media.findByPk(id);

    if (!media) {
        return res.status(404).json({
            status: "error",
            message: "media not found",
        });
    }

    fs.unlink(`./public/${media.image}`, async (err) => {
        if (err) {
            return res.status(400).json({
                status: "error",
                message: err.message,
            });
        }

        await media.destroy();

        return res.json({
            status: "success",
            message: "media deleted",
        });
    });
});

function formattedToday() {
    const today = new Date();
    const yyyy = today.getFullYear();
    let mm = today.getMonth() + 1;
    let dd = today.getDate();

    if (dd < 10) dd = "0" + dd;
    if (mm < 10) mm = "0" + mm;

    return `${yyyy}${mm}${dd}`;
}

module.exports = router;
